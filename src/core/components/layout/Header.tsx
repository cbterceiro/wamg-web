import { FunctionComponent, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import classNames from 'classnames';

import { selectIsUserAuthenticated, selectUsername } from "../../../redux/selectors";
import { userSignOut } from "../../../redux/actions/userActions";

const Header: FunctionComponent = () => {
  const username = useSelector(selectUsername);
  const isUserAuthenticated = useSelector(selectIsUserAuthenticated);
  const dispatch = useDispatch();

  const [isMenuActive, setIsMenuActive] = useState<boolean>(false);

  const signOut = () => {
    dispatch(userSignOut())
    setIsMenuActive(false);
  };

  return (
    <nav className="navbar is-dark is-fixed-top">
      <div className="container">
        <div className="navbar-brand">
          <a className="navbar-item has-text-weight-semibold" href="/">
            WAMG
          </a>
          {isUserAuthenticated && (
            <div className={classNames('navbar-burger', { 'is-active': isMenuActive })} onClick={() => setIsMenuActive(!isMenuActive)}>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </div>
          )}
        </div>
        {isUserAuthenticated && (
          <div className={classNames('navbar-menu', { 'is-active': isMenuActive })}>
            <div className="navbar-start has-text-centered is-hidden-desktop">
              <div className="navbar-item">
                <span className="icon">
                  <span className="fas fa-user-circle"></span>
                </span>
                <span>{username}</span>
              </div>
              <div className="navbar-item">
                <button type="button" className="button is-light is-fullwidth" onClick={signOut}>
                  <span className="icon">
                    <span className="fas fa-door-open"></span>
                  </span>
                  <span>Sign out</span>
                </button>
              </div>
            </div>
            <div className="navbar-end is-hidden-touch">
              <div className="navbar-item">
                Hello, {username}!
              </div>
              <div className="navbar-item">
                <button type="button" className="button is-small is-primary" onClick={signOut}>
                  Sign out
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    </nav>
  );
}

export default Header;
