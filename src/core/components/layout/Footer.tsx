import { FunctionComponent } from "react";

const Footer: FunctionComponent = () => {
  return (
    <footer className="footer has-background-dark has-text-white p-2">
      <div className="content has-text-centered">
        <small>
          <p>
            <span>Developed by <i>César C. B. Terceiro</i></span>
            <a className="has-text-white" href="https://linkedin.com/in/cesar-terceiro/" target="_blank" rel="noopener noreferrer">
              <span className="icon">
                <span className="fab fa-linkedin"></span>
              </span>
            </a>
          </p>
        </small>
      </div>
    </footer>
  );
}

export default Footer;
