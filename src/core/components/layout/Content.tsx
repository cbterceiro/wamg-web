import { FunctionComponent } from "react";

const Content: FunctionComponent = ({ children }) => {
  return (
    <div className="content-wrapper">
      {children}
    </div>
  );
}

export default Content;
