import { FunctionComponent } from "react";

import Header from "./Header";
import Content from "./Content";
import Footer from "./Footer";

const Layout: FunctionComponent = ({ children }) => {
  return (
    <div className="main-wrapper">
      <Header />
      <Content>
        {children}
      </Content>
      <Footer />
    </div>
  );
}

export default Layout;
