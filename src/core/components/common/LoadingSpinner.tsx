import { FunctionComponent } from "react";

type LoadingSpinnerProps = {
  isLoading?: boolean;
};

const LoadingSpinner: FunctionComponent<LoadingSpinnerProps> = ({ isLoading, children }) => {
  return (
    isLoading ? (
      <div className="has-text-centered is-size-7">
        <span className="icon">
          <span className="fas fa-spinner fa-pulse"></span>
        </span>
        {children}
      </div>
    ) : (
      <></>
    )
  );
}

export default LoadingSpinner;
