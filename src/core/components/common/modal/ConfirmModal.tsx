import React, { FunctionComponent } from "react";

import Modal from "./Modal";
import ModalCardBody from "./ModalCardBody";
import ModalCardFooter from "./ModalCardFooter";
import ModalCardHeader from "./ModalCardHeader";

type ConfirmModalProps = {
  visible: boolean;
  setVisible: (visible: boolean) => any;
  title: string;
  message: string;
  onConfirm: () => any;
  onCancel: () => any;
  isConfirming?: boolean;
};

const ConfirmModal: FunctionComponent<ConfirmModalProps> = ({ visible, setVisible, title, message, onConfirm, onCancel, isConfirming }) => {
  const close = () => {
    setVisible(false);
    onCancel && onCancel();
  }

  return (
    <Modal card visible={visible} setVisible={setVisible} width="450px">
      <ModalCardHeader title={title} onClose={close} />
      <ModalCardBody>
        <div className="has-text-centered">
          {message}
        </div>
      </ModalCardBody>
      <ModalCardFooter
        onConfirm={onConfirm}
        onCancel={close}
        isConfirming={isConfirming}
      />
    </Modal>
  );
}

export default ConfirmModal;
