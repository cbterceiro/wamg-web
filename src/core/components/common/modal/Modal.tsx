import { FunctionComponent } from "react";
import classNames from 'classnames';

type ModalProps = {
  visible: boolean;
  setVisible: (visible: boolean) => any;
  hasCloseButton?: boolean;
  card?: boolean;
  width?: string;
};

const Modal: FunctionComponent<ModalProps> = ({ visible, setVisible, hasCloseButton, card, width, children }) => {
  const close = () => {
    setVisible(false);
  };

  return (
    <div className={classNames('modal', { 'is-active': visible })}>
      <div className="modal-background animated fadeIn faster" onClick={close}></div>
      <div className={classNames('animated fadeInUp faster', { 'modal-content': !card, 'card': card })} style={{ width: width || 'auto' }}>
        {children}
      </div>
      {hasCloseButton && (
        <button type="button" className="modal-close is-large" onClick={close}></button>
      )}
    </div>
  );
}

export default Modal;
