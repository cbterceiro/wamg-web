import { FunctionComponent } from "react";

import classNames from "classnames";

type ModalCardFooterProps = {
  onConfirm: () => any;
  onCancel: () => any;
  isConfirming?: boolean;
  isCanceling?: boolean;
  confirmButtonLabel?: string;
  cancelButtonLabel?: string;
};

const ModalCardFooter: FunctionComponent<ModalCardFooterProps> = ({ onConfirm, onCancel, isConfirming, isCanceling, confirmButtonLabel = 'Confirm', cancelButtonLabel = 'Cancel' }) => {
  return (
    <footer className="modal-card-foot is-justify-content-center">
      <button type="button" className={classNames('button is-primary', { 'is-loading': isConfirming })} onClick={onConfirm}>{confirmButtonLabel}</button>
      <button type="button" className={classNames('button', { 'is-loading': isCanceling })} onClick={onCancel}>{cancelButtonLabel}</button>
    </footer>
  );
}

export default ModalCardFooter;
