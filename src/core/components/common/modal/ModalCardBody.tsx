import { FunctionComponent } from "react";

const ModalCardBody: FunctionComponent = ({ children }) => {
  return (
    <section className="modal-card-body">
      {children}
    </section>
  );
}

export default ModalCardBody;
