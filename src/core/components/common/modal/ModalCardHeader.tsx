import { FunctionComponent } from "react";

type ModalCardHeaderProps = {
  title: string;
  onClose: () => any;
};

const ModalCardHeader: FunctionComponent<ModalCardHeaderProps> = ({ title, onClose }) => {
  return (
    <header className="modal-card-head has-text-centered">
      <p className="modal-card-title">{title}</p>
      <button type="button" className="delete" aria-label="close" onClick={onClose}></button>
    </header>
  );
}

export default ModalCardHeader;
