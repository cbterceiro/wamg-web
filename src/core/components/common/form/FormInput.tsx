import { FunctionComponent } from "react";
import classNames from "classnames";
import { DeepMap, FieldError, RegisterOptions } from "react-hook-form";

type FormInputProps = {
  type?: 'text' | 'password' | 'checkbox';
  name: string;
  label: string;
  register: any;
  errors: DeepMap<any, FieldError>;
  rules?: RegisterOptions;
};

const FormInput: FunctionComponent<FormInputProps> = (props) => {
  return (
    props.type === 'checkbox'
      ? <CheckboxFormInput {...props} />
      : <DefaultFormInput {...props} />
  );
}

const DefaultFormInput: FunctionComponent<FormInputProps> = ({ type, name, label, register, errors, rules }) => {
  return (
    <div className="field">
      <label className="label">{label}</label>
      <div className="control">
        <input type={type} className={classNames('input', { 'is-danger': errors[name] })} ref={register(rules)} name={name} />

        {Object.keys(rules || {}).map(ruleName => (
          errors[name]?.type === ruleName && (<p className="help is-danger">{errors[name]?.message}</p>)
        ))}
      </div>
    </div>
  );
};

const CheckboxFormInput: FunctionComponent<FormInputProps> = ({ name, label, register, errors, rules }) => {
  return (
    <div className="field">
      <div className="control">
        <label className="checkbox">
          <input type="checkbox" className={classNames('mr-2', { 'is-danger': errors[name] })} ref={register(rules)} name={name} />
          {label}
        </label>
      </div>
    </div>
  );
};

export default FormInput;
