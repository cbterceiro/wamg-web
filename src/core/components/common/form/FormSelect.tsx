import { FunctionComponent } from "react";
import { DeepMap, FieldError, RegisterOptions } from "react-hook-form";

import classNames from "classnames";

type FormSelectProps = {
  name: string;
  label: string;
  options: any[];
  optionLabelField?: string;
  optionValueField?: string;
  showDefaultNullOption?: boolean;
  defaultNullOptionLabel?: string;
  isLoading?: boolean;
  register: any;
  errors: DeepMap<any, FieldError>;
  rules?: RegisterOptions;
};

const FormSelect: FunctionComponent<FormSelectProps> = ({ name, label, options, optionLabelField, optionValueField, showDefaultNullOption, defaultNullOptionLabel, isLoading, register, errors, rules }) => {
  const getOptionLabel = (option: any) => optionLabelField ? option[optionLabelField] : option;
  const getOptionValue = (option: any) => optionValueField ? option[optionValueField] : option;

  return (
    <div className="field">
      <label className="label">{label}</label>
      <div className="control">
        <div className={classNames('select is-fullwidth', { 'is-danger': errors[name], 'is-loading': isLoading })}>
          <select name={name} ref={register(rules)}>
            {showDefaultNullOption && (<option value="">{defaultNullOptionLabel || ' '}</option>)}
            {(options || []).map(option => (
              <option key={getOptionLabel(option)} value={getOptionValue(option)}>
                {getOptionLabel(option)}
              </option>
            ))}
          </select>
        </div>

        {Object.keys(rules || {}).map(ruleName => (
          errors[name]?.type === ruleName && (<p className="help is-danger">{errors[name]?.message}</p>)
        ))}
      </div>
    </div>
  );
}

export default FormSelect;
