import { useEffect } from "react";

export function useEffectOnUnmount(callback: () => any): void {
  // eslint-disable-next-line
  useEffect(() => callback, []);
}
