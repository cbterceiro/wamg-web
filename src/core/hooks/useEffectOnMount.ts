import { useEffect, EffectCallback } from "react";

export function useEffectOnMount(callback: EffectCallback): void {
  // eslint-disable-next-line
  useEffect(callback, []);
}
