export type GamingPlatform = {
  id: number;
  name: string;
  isConsole: boolean;
  isHandheld: boolean;
};
