import { Friend } from "./friend";
import { GamingPlatform } from "./gamingPlatform";

export type Game = {
  id: number;
  name: string;
  genre: string;
  platform: GamingPlatform;
  loanedTo: Friend;
};
