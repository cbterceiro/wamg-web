export type Friend = {
  id: number;
  name: string;
  email: string;
  phoneNumber: string;
}
