export type PaginationResult<T> = {
  pageNumber: number;
  pageSize: number;
  items: T[];
  totalPages: number;
  totalCount: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
};
