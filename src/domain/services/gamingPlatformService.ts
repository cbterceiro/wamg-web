import { AxiosInstance } from "axios";
import { GamingPlatform } from "../models/gamingPlatform";

export class GamingPlatformService {

  constructor(
    private axios: AxiosInstance,
  ) { }

  getGamingPlatforms() {
    return this.axios.get<GamingPlatform[]>('/gamingPlatform').then(res => res.data);
  }

  getGamingPlatformById(gamingPlatformId: number) {
    return this.axios.get<GamingPlatform>(`/gamingPlatform/${gamingPlatformId}`).then(res => res.data);
  }

  addGamingPlatform(gamingPlatform: GamingPlatform) {
    return this.axios.post('/gamingPlatform', gamingPlatform);
  }

  updateGamingPlatform(gamingPlatformId: number, gamingPlatform: GamingPlatform) {
    return this.axios.put(`/gamingPlatform/${gamingPlatformId}`, gamingPlatform);
  }

  removeGamingPlatform(gamingPlatformId: number) {
    return this.axios.delete(`/gamingPlatform/${gamingPlatformId}`);
  }
}
