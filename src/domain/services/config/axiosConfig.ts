import axios from "axios";

import { configureRequestInterceptors } from "./axiosRequestInterceptor";
import { configureResponseInterceptors } from "./axiosResponseInterceptor";

const axiosConfig = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

configureRequestInterceptors(axiosConfig);
configureResponseInterceptors(axiosConfig);

export default axiosConfig;
