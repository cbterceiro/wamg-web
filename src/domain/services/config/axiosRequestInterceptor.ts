import { AxiosInstance } from "axios";

import store from "../../../redux/store";
import { selectAccessToken } from "../../../redux/selectors";

export const configureRequestInterceptors = (axiosConfig: AxiosInstance) => {
  axiosConfig.interceptors.request.use(
    config => {
      const accessToken = selectAccessToken(store.getState());

      if (accessToken) {
        config.headers.Authorization = `Bearer ${accessToken}`;
      }

      return config;
    },
    error => Promise.reject(error),
  );
};
