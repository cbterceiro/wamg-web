import { AxiosInstance } from "axios";

import { toastr } from "react-redux-toastr";

import store from "../../../redux/store";
import { userSignOut } from "../../../redux/actions/userActions";

export const configureResponseInterceptors = (axiosConfig: AxiosInstance) => {
  axiosConfig.interceptors.response.use(
    response => response,
    error => {

      // Check user's connection
      const isConnectionOffline = error.toString() === 'Error: Network Error';

      if (isConnectionOffline) {
        toastr.error('Error', 'Can\'t connect with application servers. Please contact systems admin.', { timeOut: 0, removeOnHover: false });
        return Promise.reject(error);
      }

      // Check authentication status
      const isUserUnauthorized = error.response && error.response.status === 401;
      const isLoginRequest = error.request && error.request.responseURL.endsWith('/user/authenticate');

      if (isUserUnauthorized) {
        if (isLoginRequest) {
          toastr.error('Authentication error', 'Invalid username or password', { removeOnHover: false, });
        } else {
          toastr.error('Session', 'Session expired, please sign in again.', {
            attention: true,
            timeOut: 15000,
            onHideComplete: () => {
              store.dispatch(userSignOut());
            }
          });
        }
        return Promise.reject(error);
      }

      // Display error messages
      const errorTitle = error.response.data?.title || 'Error';
      const hasErrors = Boolean(error.response.data?.errors);

      if (hasErrors) {
        const errors = error.response.data.errors;
        const errorMessages = Object.keys(errors)
          .map(field => {
            const fieldErrorMessages = errors[field];
            return [...fieldErrorMessages].join('\n');
          })
          .join('\n');
        toastr.error(errorTitle, errorMessages);
      } else {
        toastr.error(errorTitle, '');
      }

      return Promise.reject(error);
    },
  );
}

