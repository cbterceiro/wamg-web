import { AxiosInstance } from "axios";
import { PaginationResult } from "../models/common/paginationResult";
import { Friend } from "../models/friend";

export class FriendService {

  constructor(
    private axios: AxiosInstance,
  ) { }

  getFriends() {
    // on a real app, we'd use pagination coming from user input, here we're simplifying things just to demonstrate
    return this.axios.get<PaginationResult<Friend>>('/friend?pageNumber=1&pageSize=100').then(res => res.data.items);
  }

  getFriendById(friendId: number) {
    return this.axios.get<Friend>(`/friend/${friendId}`).then(res => res.data);
  }

  addFriend(friend: Friend) {
    return this.axios.post('/friend', this.createFriendRequest(friend));
  }

  updateFriend(friendId: number, friend: Friend) {
    return this.axios.put(`/friend/${friendId}`, this.createFriendRequest(friend));
  }

  removeFriend(friendId: number) {
    return this.axios.delete(`/friend/${friendId}`);
  }

  private createFriendRequest(friend: Friend): Partial<Friend> {
    const friendRequest: Partial<Friend> = { ...friend };
    if (!friend.email) delete friendRequest.email;
    if (!friend.phoneNumber) delete friendRequest.phoneNumber;
    return friendRequest;
  }
}
