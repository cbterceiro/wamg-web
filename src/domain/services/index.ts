import axiosConfig from "./config/axiosConfig";

import { AuthService } from "./authService";
import { FriendService } from "./friendService";
import { GamingPlatformService } from "./gamingPlatformService";
import { GameService } from "./gameService";

export type ApiServicesType = typeof ApiServices;
export type ApiServicesKeys = keyof ApiServicesType;

export const ApiServices = {
  auth: new AuthService(axiosConfig),
  friend: new FriendService(axiosConfig),
  gamingPlatform: new GamingPlatformService(axiosConfig),
  game: new GameService(axiosConfig),
};
