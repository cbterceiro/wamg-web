import { AxiosInstance } from "axios";

type SignInRequestDTO = {
  username: string;
  password: string;
};

type SignInResponseDTO = {
  accessToken: string;
};

export class AuthService {

  constructor(
    private axios: AxiosInstance,
  ) { }

  signIn(req: SignInRequestDTO) {
    return this.axios.post<SignInResponseDTO>('/user/authenticate', req).then(res => res.data);
  }
}
