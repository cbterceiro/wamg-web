import { AxiosInstance } from "axios";
import { PaginationResult } from "../models/common/paginationResult";
import { Game } from "../models/game";

type GameRequestDTO = {
  name: string;
  genre: string;
  platformId: number | string;
};
type AddGameRequestDTO = GameRequestDTO;
type UpdateGameRequestDTO = GameRequestDTO & { id: number };

export class GameService {

  constructor(
    private axios: AxiosInstance,
  ) { }

  getGameGenres() {
    return this.axios.get<string[]>('/game/genres').then(res => res.data);
  }

  getGames() {
    // on a real app, we'd use pagination coming from user input, here we're simplifying things just to demonstrate
    return this.axios.get<PaginationResult<Game>>('/game?pageNumber=1&pageSize=100').then(res => res.data.items);
  }

  getGameById(gameId: number) {
    return this.axios.get<Game>(`/game/${gameId}`).then(res => res.data);
  }

  addGame(game: AddGameRequestDTO) {
    return this.axios.post('/game', game);
  }

  updateGame(gameId: number, game: UpdateGameRequestDTO) {
    return this.axios.put(`/game/${gameId}`, game);
  }

  loanGameToFriend(gameId: number, friendId: number) {
    return this.axios.patch(`/game/${gameId}/loanTo/${friendId}`);
  }

  returnGameFromFriend(gameId: number) {
    return this.axios.patch(`/game/${gameId}/return`);
  }

  removeGame(gameId: number) {
    return this.axios.delete(`/game/${gameId}`);
  }
}
