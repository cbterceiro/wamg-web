import { ApiServices, ApiServicesKeys, ApiServicesType } from "../index";
import { useState, useEffect, useRef, useCallback } from "react";
import { useEffectOnUnmount } from "../../../core/hooks/useEffectOnUnmount";
import { useEffectOnMount } from "../../../core/hooks/useEffectOnMount";


type UseApiServiceReturnType<U> = [
  (...params: U[]) => void,
  { isLoading: boolean },
];

type RequestState = {
  isLoading: boolean,
  apiServiceParams?: any[],
};

export function useApiService<K extends ApiServicesKeys, U = any, V = any>(name: K, requestFn: (service: ApiServicesType[K], ...params: U[]) => Promise<V>, onSuccess: (returnValue: V) => any, onError?: (error: any) => any): UseApiServiceReturnType<U> {
  const service = ApiServices[name];

  const [requestState, setRequestState] = useState<RequestState>({ isLoading: false, apiServiceParams: undefined });
  const didCancel = useRef(false);
  const reqHandler = useCallback((...params: U[]) => {
    setRequestState({ isLoading: true, apiServiceParams: params });
  // eslint-disable-next-line
  }, []);

  useEffect(() => {
    const { isLoading, apiServiceParams } = requestState;

    if (!isLoading) {
      return;
    }

    const _onSuccess = (returnValue: V) => {
      if (!didCancel.current) {
        return onSuccess(returnValue);
      }
    };

    const _onError = (error: any) => {
      return (onError && onError(error)) || undefined;
    }

    const _onFinally = () => {
      if (!didCancel.current) {
        setRequestState({ isLoading: false });
      }
    }

    // calling the API
    requestFn(service, ...(apiServiceParams || []))
      .then(_onSuccess)
      .catch(_onError)
      .finally(_onFinally);

  // eslint-disable-next-line
  }, [requestState]);

  useEffectOnMount(() => {
    didCancel.current = false;
  });

  useEffectOnUnmount(() => {
    didCancel.current = true;
  });

  return [reqHandler, { isLoading: requestState.isLoading }];
}
