import { ComponentType, FunctionComponent } from "react";
import { useSelector } from "react-redux";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import Layout from "../core/components/layout/Layout";
import ScrollToTop from "../core/components/layout/ScrollToTop";

import { selectUser } from "../redux/selectors";
import { UserState } from "../redux/types";

import Error404Page from "./error-handling/Error404Page";
import routeConfig, { AppRoute } from "./routerConfig";


type RouteCreator = (index: string, path: string, component: ComponentType, user: UserState) => JSX.Element;

const createRoute: RouteCreator = (index, path, Component) => (
  <Route
    key={index}
    exact
    path={path}
    render={() => (
      <Layout>
        <Component />
      </Layout>
    )}
  />
);

const createProtectedRoute: RouteCreator = (index, path, Component, user) => (
  user && user.isAuthenticated
    ? createRoute(index, path, Component, user)
    : createRedirectToLoginRoute(index)
);

const createRedirectToLoginRoute = (index: string) => (
  <Route
    key={index}
    render={props => (
      <Redirect to={{
        pathname: '/auth/sign-in',
        state: {
          from: props.location,
        },
      }} />
    )}
  />
);

const flattenTree = (root: AppRoute) => {
  let flatten: AppRoute[] = [{ ...root }];

  if (root.children && root.children.length > 0) {
    const flattenedChildren: AppRoute[] = root.children
      .map(child => flattenTree(child))
      .reduce((a, b) => a.concat(b), []);

    return flatten.concat(flattenedChildren);
  }

  return flatten;
};

const renderRoutes = (routes: AppRoute[], user: UserState, createRouteFn: RouteCreator) => {
  return routes
    .map(r => flattenTree(r))
    .reduce((a, b) => [...a, ...b], [])
    .map(({path, component}, index) => createRouteFn(`${index}`, path, component, user));
}

const AppRouter: FunctionComponent = () => {
  const user = useSelector(selectUser);
  return (
    <Router>
      <ScrollToTop>
        <Switch>
          {renderRoutes(routeConfig.public, user, createRoute)}
          {renderRoutes(routeConfig.protected, user, createProtectedRoute)}

          <Route
            render={() => (
              <Layout><Error404Page /></Layout>
            )}
          />
        </Switch>
      </ScrollToTop>
    </Router>
  );
}

export default AppRouter;
