import { ComponentType } from "react";

import LoginPage from "./login/LoginPage";

import HomePage from "./home/HomePage";

const publicRoutes: AppRoute[] = [
  {
    path: '/auth/sign-in',
    component: LoginPage,
  },
];

const protectedRoutes: AppRoute[] = [
  {
    path: '/',
    component: HomePage,
  },
];

export type AppRoute = {
  path: string;
  component: ComponentType;
  children?: AppRoute[];
};

export type RouteConfig = {
  public: AppRoute[];
  protected: AppRoute[];
};

const routeConfig: RouteConfig = {
  public: publicRoutes,
  protected: protectedRoutes,
};

export default routeConfig;