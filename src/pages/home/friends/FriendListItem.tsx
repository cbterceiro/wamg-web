import { FunctionComponent } from "react";
import { Friend } from "../../../domain/models/friend";

type FriendListItemProps = {
  friend: Friend;
  onEditButtonClick: (friend: Friend) => void;
  onRemoveButtonClick: (friend: Friend) => void;
};

const FriendListItem: FunctionComponent<FriendListItemProps> = ({ friend, onEditButtonClick, onRemoveButtonClick }) => {
  return (
    <div className="level mb-0 py-2" style={{ borderBottom: '1px solid #dedede' }}>
      <div className="level-left">
        {friend.name}
      </div>
      <div className="level-right">
        <div className="buttons has-addons">
          <button type="button" className="button is-small is-light" onClick={() => onEditButtonClick(friend)}>
            <span className="icon">
              <span className="fas fa-pen"></span>
            </span>
          </button>
          <button type="button" className="button is-small is-light" onClick={() => onRemoveButtonClick(friend)}>
            <span className="icon">
              <span className="fas fa-trash"></span>
            </span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default FriendListItem;
