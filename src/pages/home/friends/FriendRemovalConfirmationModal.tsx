import React, { FunctionComponent } from "react";
import { toastr } from "react-redux-toastr";
import ConfirmModal from "../../../core/components/common/modal/ConfirmModal";
import { Friend } from "../../../domain/models/friend";
import { useApiService } from "../../../domain/services/hooks/useApiService";

type FriendRemovalConfirmationModalProps = {
  visible: boolean;
  setVisible: (visible: boolean) => any;
  friend?: Friend;
  refetch: () => void;
};

const FriendRemovalConfirmationModal: FunctionComponent<FriendRemovalConfirmationModalProps> = ({ visible, setVisible, friend, refetch }) => {
  const [removeFriend, { isLoading }] = useApiService('friend',
    (friendService) => friendService.removeFriend(friend!.id),
    () => {
      toastr.success('Success', `Friend removed successfully`);
      setVisible(false);
      refetch();
    }
  );

  return (
    <ConfirmModal
      visible={visible}
      setVisible={setVisible}
      title="Remove friend"
      message={`Do you really want to remove friend "${friend?.name}"?`}
      isConfirming={isLoading}
      onConfirm={() => removeFriend()}
      onCancel={() => setVisible(false)}
    />
  );
}

export default FriendRemovalConfirmationModal;


