import { FunctionComponent, useEffect } from "react";
import { toastr } from "react-redux-toastr";
import { useForm } from "react-hook-form";

import emailRegex from "email-regex";

import Modal from "../../../core/components/common/modal/Modal";
import ModalCardHeader from "../../../core/components/common/modal/ModalCardHeader";
import ModalCardBody from "../../../core/components/common/modal/ModalCardBody";
import ModalCardFooter from "../../../core/components/common/modal/ModalCardFooter";

import FormInput from "../../../core/components/common/form/FormInput";

import { Friend } from "../../../domain/models/friend";
import { useApiService } from "../../../domain/services/hooks/useApiService";

type FriendModalProps = {
  friendModalData?: Friend;
  visible: boolean;
  setVisible: (visible: boolean) => any;
  refetch: () => void
};

type FriendFormData = Friend;

const emailRegExp = emailRegex({ exact: true });

const FriendFormModal: FunctionComponent<FriendModalProps> = ({ friendModalData, visible, setVisible, refetch }) => {

  const { register, handleSubmit, errors, reset } = useForm<FriendFormData>({ mode: 'onTouched' });

  const friendId = friendModalData?.id;

  const handleSaveFriendSuccess = () => {
    toastr.success('Success', 'Friend saved successfully!');
    close();
    refetch();
  };

  const [addFriend, { isLoading: isAddingFriend }] = useApiService('friend',
    (friendService, friend: Friend) => friendService.addFriend(friend),
    handleSaveFriendSuccess,
  );
  const [updateFriend, { isLoading: isUpdatingFriend }] = useApiService('friend',
    (friendService, friend: Friend) => friendService.updateFriend(friend.id, friend),
    handleSaveFriendSuccess,
  );

  const isSaving = isAddingFriend || isUpdatingFriend;

  const onSubmit = handleSubmit(data => {
    friendId
      ? updateFriend({ ...data, id: friendId })
      : addFriend(data);
  });

  const close = () => {
    setVisible(false);
  };

  useEffect(() => {
    if (visible) {
      reset({ ...(friendModalData || {}) });
    } else {
      reset();
    }
  // eslint-disable-next-line
  }, [visible]);

  return (
    <Modal card visible={visible} setVisible={setVisible} width="500px">
      <ModalCardHeader title={friendId ? 'Update friend' : 'Add friend'} onClose={close} />
      <ModalCardBody>
        <form onSubmit={onSubmit}>
          <FormInput
            name="name"
            label="Name *"
            register={register}
            errors={errors}
            rules={{
              required: { value: true, message: 'The friend\'s name is required' },
              maxLength: { value: 100, message: 'The friend\'s name must not exceed 100 characters' }
            }}
          />
          <FormInput
            name="email"
            label="E-mail"
            register={register}
            errors={errors}
            rules={{
              pattern: { value: emailRegExp, message: 'The friend\'s email address must be a valid one' },
              maxLength: { value: 50, message: 'The friend\'s email address must not exceed 50 characters' }
            }}
          />
          <FormInput
            name="phoneNumber"
            label="Phone number"
            register={register}
            errors={errors}
            rules={{
              maxLength: { value: 20, message: 'The friend\'s phone number must not exceed 20 characters' }
            }}
          />
        </form>
      </ModalCardBody>
      <ModalCardFooter onConfirm={onSubmit} onCancel={close} isConfirming={isSaving} confirmButtonLabel="Save" />
    </Modal>
  );
}

export default FriendFormModal;
