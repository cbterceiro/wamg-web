import { FunctionComponent, useState } from "react";

import { useEffectOnMount } from "../../../core/hooks/useEffectOnMount";

import { Friend } from "../../../domain/models/friend";

import SectionSubtitle from "../SectionSubtitle";

import FriendListItem from "./FriendListItem";
import FriendFormModal from "./FriendFormModal";
import FriendRemovalConfirmationModal from "./FriendRemovalConfirmationModal";

type FriendsListProps = {
  friendsList: Friend[];
  isLoading: boolean;
  fetchFriends: () => any;
  fetchGames: () => any;
};

const FriendList: FunctionComponent<FriendsListProps> = ({ friendsList, isLoading, fetchFriends, fetchGames }) => {
  const [friendModalVisible, setFriendModalVisible] = useState<boolean>(false);
  const [friendModalData, setFriendModalData] = useState<Friend>();

  const [removeFriendConfirmModalVisible, setRemoveFriendConfirmModalVisible] = useState<boolean>(false);
  const [friendBeingRemoved, setFriendBeingRemoved] = useState<Friend>();

  const openAddFriendModal = () => {
    setFriendModalData(undefined);
    setFriendModalVisible(true);
  };

  const openEditFriendModal = (friend: Friend) => {
    setFriendModalData(friend);
    setFriendModalVisible(true);
  };

  const openRemoveFriendConfirmationModal = (friend: Friend) => {
    setFriendBeingRemoved(friend);
    setRemoveFriendConfirmModalVisible(true);
  };

  useEffectOnMount(() => {
    fetchFriends();
  });

  return (
    <>
      <SectionSubtitle subtitle="My friends" isLoading={isLoading} />

      <div className="content">
        <div className="mb-2">
          <button type="button" className="button is-primary is-fullwidth is-small has-icons-left" onClick={openAddFriendModal}>
            <span className="icon">
              <span className="fas fa-plus"></span>
            </span>
            <span>Add friend</span>
          </button>
        </div>

        {friendsList.length ? (
          <div>
            {friendsList.map(friend => (
              <FriendListItem
                key={friend.id}
                friend={friend}
                onEditButtonClick={openEditFriendModal}
                onRemoveButtonClick={openRemoveFriendConfirmationModal}
              />
            ))}
          </div>
        ) : (
          <div className="block has-text-centered">
            <small>You currently have no friends added</small>
          </div>
        )}
      </div>

      <FriendFormModal
        friendModalData={friendModalData}
        visible={friendModalVisible}
        setVisible={setFriendModalVisible}
        refetch={() => { fetchFriends(); fetchGames(); }}
      />
      <FriendRemovalConfirmationModal
        friend={friendBeingRemoved}
        visible={removeFriendConfirmModalVisible}
        setVisible={setRemoveFriendConfirmModalVisible}
        refetch={() => { fetchFriends(); fetchGames(); }}
      />
    </>
  );
}

export default FriendList;
