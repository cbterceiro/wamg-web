import { FunctionComponent } from "react";

import LoadingSpinner from "../../core/components/common/LoadingSpinner";

type SectionSubtitleProps = {
  subtitle: string;
  isLoading?: boolean;
}

const SectionSubtitle: FunctionComponent<SectionSubtitleProps> = ({ subtitle, isLoading }) => {
  return (
    <>
      <div className="level mb-0">
        <div className="level-left">
          <h2 className="subtitle mb-0">{subtitle}</h2>
        </div>
        <div className="level-right">
          <span className="ml-auto"><LoadingSpinner isLoading={isLoading} /></span>
        </div>
      </div>
      <hr />
    </>
  );
}

export default SectionSubtitle;
