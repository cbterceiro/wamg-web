import { FunctionComponent, useState } from "react";

import { Friend } from "../../domain/models/friend";
import { Game } from "../../domain/models/game";
import { GamingPlatform } from "../../domain/models/gamingPlatform";
import { useApiService } from "../../domain/services/hooks/useApiService";

import FriendList from "./friends/FriendList";
import GameList from "./games/GameList";
import GamingPlatformList from "./gaming-platforms/GamingPlatformList";

const HomePage: FunctionComponent = () => {
  const [friendsList, setFriendsList] = useState<Friend[]>([]);
  const [gamesList, setGamesList] = useState<Game[]>([]);
  const [gamingPlatformsList, setGamingPlatformsList] = useState<GamingPlatform[]>([]);

  const [getFriends, { isLoading: isLoadingFriends }] = useApiService('friend',
    (friendService) => friendService.getFriends(),
    (friends) => setFriendsList(friends),
  );

  const [getGames, { isLoading: isLoadingGames }] = useApiService('game',
    (gameService) => gameService.getGames(),
    (games) => setGamesList(games),
  );

  const [getGamingPlatforms, { isLoading: isLoadingGamingPlatforms }] = useApiService('gamingPlatform',
    (gamingPlatformService) => gamingPlatformService.getGamingPlatforms(),
    (gamingPlatforms) => setGamingPlatformsList(gamingPlatforms),
  );

  return (
    <div className="container p-4">
      <div className="columns">
        <div className="column is-one-quarter">
          <FriendList
            friendsList={friendsList}
            isLoading={isLoadingFriends}
            fetchFriends={getFriends}
            fetchGames={getGames}
          />
        </div>
        <div className="column">
          <GameList
            gamesList={gamesList}
            isLoading={isLoadingGames}
            fetchGames={getGames}

            gamingPlatformsList={gamingPlatformsList}
            isLoadingGamingPlatforms={isLoadingGamingPlatforms}

            friendsList={friendsList}
            isLoadingFriends={isLoadingFriends}
          />
        </div>
        <div className="column is-one-quarter">
          <GamingPlatformList
            gamingPlatformsList={gamingPlatformsList}
            isLoading={isLoadingGamingPlatforms}
            fetchGamingPlatforms={getGamingPlatforms}
            fetchGames={getGames}
          />
        </div>
      </div>
    </div>
  );
}

export default HomePage;
