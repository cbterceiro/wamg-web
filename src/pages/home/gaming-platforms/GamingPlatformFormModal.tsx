import { FunctionComponent, useEffect } from "react";
import { toastr } from "react-redux-toastr";
import { useForm } from "react-hook-form";

import Modal from "../../../core/components/common/modal/Modal";
import ModalCardHeader from "../../../core/components/common/modal/ModalCardHeader";
import ModalCardBody from "../../../core/components/common/modal/ModalCardBody";
import ModalCardFooter from "../../../core/components/common/modal/ModalCardFooter";

import FormInput from "../../../core/components/common/form/FormInput";

import { GamingPlatform } from "../../../domain/models/gamingPlatform";
import { useApiService } from "../../../domain/services/hooks/useApiService";

type GamingPlatformModalProps = {
  gamingPlatformModalData?: GamingPlatform;
  visible: boolean;
  setVisible: (visible: boolean) => any;
  refetch: () => void
};

type GamingPlatformFormData = GamingPlatform;

const GamingPlatformFormModal: FunctionComponent<GamingPlatformModalProps> = ({ gamingPlatformModalData, visible, setVisible, refetch }) => {

  const { register, handleSubmit, errors, reset } = useForm<GamingPlatformFormData>({ mode: 'onTouched' });

  const gamingPlatformId = gamingPlatformModalData?.id;

  const handleSaveGamingPlatformSuccess = () => {
    toastr.success('Success', 'GamingPlatform saved successfully!');
    close();
    refetch();
  };

  const [addGamingPlatform, { isLoading: isAddingGamingPlatform }] = useApiService('gamingPlatform',
    (gamingPlatformService, gamingPlatform: GamingPlatform) => gamingPlatformService.addGamingPlatform(gamingPlatform),
    handleSaveGamingPlatformSuccess,
  );
  const [updateGamingPlatform, { isLoading: isUpdatingGamingPlatform }] = useApiService('gamingPlatform',
    (gamingPlatformService, gamingPlatform: GamingPlatform) => gamingPlatformService.updateGamingPlatform(gamingPlatform.id, gamingPlatform),
    handleSaveGamingPlatformSuccess,
  );

  const isSaving = isAddingGamingPlatform || isUpdatingGamingPlatform;

  const onSubmit = handleSubmit(data => {
    gamingPlatformId
      ? updateGamingPlatform({ ...data, id: gamingPlatformId })
      : addGamingPlatform(data);
  });

  const close = () => {
    setVisible(false);
  };

  useEffect(() => {
    if (visible) {
      reset({ ...(gamingPlatformModalData || {}) });
    } else {
      reset();
    }
  // eslint-disable-next-line
  }, [visible]);

  return (
    <Modal card visible={visible} setVisible={setVisible} width="500px">
      <ModalCardHeader title={gamingPlatformId ? 'Update gaming platform' : 'Add gaming platform'} onClose={close} />
      <ModalCardBody>
        <form onSubmit={onSubmit}>
          <FormInput
            name="name"
            label="Name *"
            register={register}
            errors={errors}
            rules={{
              required: { value: true, message: 'The gaming platform\'s name is required' },
              maxLength: { value: 100, message: 'The gaming platform\'s name must not exceed 100 characters' }
            }}
          />
          <FormInput
            type="checkbox"
            name="isConsole"
            label="Is it a gaming console?"
            register={register}
            errors={errors}
          />
          <FormInput
            type="checkbox"
            name="isHandheld"
            label="Is it a handheld gaming platform?"
            register={register}
            errors={errors}
          />
        </form>
      </ModalCardBody>
      <ModalCardFooter onConfirm={onSubmit} onCancel={close} isConfirming={isSaving} confirmButtonLabel="Save" />
    </Modal>
  );
}

export default GamingPlatformFormModal;
