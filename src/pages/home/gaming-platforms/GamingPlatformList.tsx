import { FunctionComponent, useState } from "react";

import { useEffectOnMount } from "../../../core/hooks/useEffectOnMount";

import { GamingPlatform } from "../../../domain/models/gamingPlatform";

import SectionSubtitle from "../SectionSubtitle";

import GamingPlatformListItem from "./GamingPlatformListItem";
import GamingPlatformFormModal from "./GamingPlatformFormModal";
import GamingPlatformRemovalConfirmationModal from "./GamingPlatformRemovalConfirmationModal";

type GamingPlatformListProps = {
  gamingPlatformsList: GamingPlatform[];
  isLoading: boolean;
  fetchGamingPlatforms: () => void;
  fetchGames: () => void;
};

const GamingPlatformsList: FunctionComponent<GamingPlatformListProps> = ({ gamingPlatformsList, isLoading, fetchGamingPlatforms, fetchGames }) => {
  const [gamingPlatformModalVisible, setGamingPlatformModalVisible] = useState<boolean>(false);
  const [gamingPlatformModalData, setGamingPlatformModalData] = useState<GamingPlatform>();

  const [removeGamingPlatformConfirmModalVisible, setRemoveGamingPlatformConfirmModalVisible] = useState<boolean>(false);
  const [gamingPlatformBeingRemoved, setGamingPlatformBeingRemoved] = useState<GamingPlatform>();

  const openAddGamingPlatformModal = () => {
    setGamingPlatformModalData(undefined);
    setGamingPlatformModalVisible(true);
  };

  const openEditGamingPlatformModal = (gamingPlatform: GamingPlatform) => {
    setGamingPlatformModalData(gamingPlatform);
    setGamingPlatformModalVisible(true);
  };

  const openRemoveGamingPlatformConfirmationModal = (gamingPlatform: GamingPlatform) => {
    setGamingPlatformBeingRemoved(gamingPlatform);
    setRemoveGamingPlatformConfirmModalVisible(true);
  };

  useEffectOnMount(() => {
    fetchGamingPlatforms();
  });


  return (
    <>
      <SectionSubtitle subtitle="My gaming platforms" isLoading={isLoading} />

      <div className="content">
        <div className="mb-2">
          <button type="button" className="button is-primary is-fullwidth is-small has-icons-left" onClick={openAddGamingPlatformModal}>
            <span className="icon">
              <span className="fas fa-plus"></span>
            </span>
            <span>Add gaming platform</span>
          </button>
        </div>

        {gamingPlatformsList.length ? (
          <div>
            {gamingPlatformsList.map(gamingPlatform => (
              <GamingPlatformListItem
                key={gamingPlatform.id}
                gamingPlatform={gamingPlatform}
                onEditButtonClick={openEditGamingPlatformModal}
                onRemoveButtonClick={openRemoveGamingPlatformConfirmationModal}
              />
            ))}
          </div>
        ) : (
          <div className="block has-text-centered">
            <small>You currently have no gaming platforms added</small>
          </div>
        )}
      </div>

      <GamingPlatformFormModal
        gamingPlatformModalData={gamingPlatformModalData}
        visible={gamingPlatformModalVisible}
        setVisible={setGamingPlatformModalVisible}
        refetch={() => { fetchGamingPlatforms(); fetchGames(); }}
      />
      <GamingPlatformRemovalConfirmationModal
        gamingPlatform={gamingPlatformBeingRemoved}
        visible={removeGamingPlatformConfirmModalVisible}
        setVisible={setRemoveGamingPlatformConfirmModalVisible}
        refetch={() => { fetchGamingPlatforms(); fetchGames(); }}
      />
    </>
  );
}

export default GamingPlatformsList;
