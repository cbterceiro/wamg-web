import { FunctionComponent } from "react";
import { GamingPlatform } from "../../../domain/models/gamingPlatform";

type GamingPlatformListItemProps = {
  gamingPlatform: GamingPlatform;
  onEditButtonClick: (gamingPlatform: GamingPlatform) => void;
  onRemoveButtonClick: (gamingPlatform: GamingPlatform) => void;
};

const GamingPlatformListItem: FunctionComponent<GamingPlatformListItemProps> = ({ gamingPlatform, onEditButtonClick, onRemoveButtonClick }) => {
  return (
    <div className="level mb-0 py-2" style={{ borderBottom: '1px solid #dedede' }}>
      <div className="level-left">
        {gamingPlatform.name}
      </div>
      <div className="level-right">
        <div className="buttons has-addons">
          <button type="button" className="button is-small is-light" onClick={() => onEditButtonClick(gamingPlatform)}>
            <span className="icon">
              <span className="fas fa-pen"></span>
            </span>
          </button>
          <button type="button" className="button is-small is-light" onClick={() => onRemoveButtonClick(gamingPlatform)}>
            <span className="icon">
              <span className="fas fa-trash"></span>
            </span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default GamingPlatformListItem;
