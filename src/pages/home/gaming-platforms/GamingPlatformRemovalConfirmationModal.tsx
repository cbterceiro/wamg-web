import React, { FunctionComponent } from "react";
import { toastr } from "react-redux-toastr";
import ConfirmModal from "../../../core/components/common/modal/ConfirmModal";
import { GamingPlatform } from "../../../domain/models/gamingPlatform";
import { useApiService } from "../../../domain/services/hooks/useApiService";

type GamingPlatformRemovalConfirmationModalProps = {
  visible: boolean;
  setVisible: (visible: boolean) => any;
  gamingPlatform?: GamingPlatform;
  refetch: () => void;
};

const GamingPlatformRemovalConfirmationModal: FunctionComponent<GamingPlatformRemovalConfirmationModalProps> = ({ visible, setVisible, gamingPlatform, refetch }) => {
  const [removeGamingPlatform, { isLoading }] = useApiService('gamingPlatform',
    (gamingPlatformService) => gamingPlatformService.removeGamingPlatform(gamingPlatform!.id),
    () => {
      toastr.success('Success', `Gaming platform removed successfully`);
      setVisible(false);
      refetch();
    }
  );

  return (
    <ConfirmModal
      visible={visible}
      setVisible={setVisible}
      title="Remove gaming platform"
      message={`Do you really want to remove gaming platform "${gamingPlatform?.name}"? It'll also remove any games associated with this platform.`}
      isConfirming={isLoading}
      onConfirm={() => removeGamingPlatform()}
      onCancel={() => setVisible(false)}
    />
  );
}

export default GamingPlatformRemovalConfirmationModal;


