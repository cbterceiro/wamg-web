import { FunctionComponent, useEffect, useState } from "react";
import { toastr } from "react-redux-toastr";
import { useForm } from "react-hook-form";

import Modal from "../../../core/components/common/modal/Modal";
import ModalCardHeader from "../../../core/components/common/modal/ModalCardHeader";
import ModalCardBody from "../../../core/components/common/modal/ModalCardBody";
import ModalCardFooter from "../../../core/components/common/modal/ModalCardFooter";

import FormInput from "../../../core/components/common/form/FormInput";
import FormSelect from "../../../core/components/common/form/FormSelect";

import { Game } from "../../../domain/models/game";
import { useApiService } from "../../../domain/services/hooks/useApiService";
import { useEffectOnMount } from "../../../core/hooks/useEffectOnMount";
import { GamingPlatform } from "../../../domain/models/gamingPlatform";

type GameModalProps = {
  gameModalData?: Game;
  visible: boolean;
  setVisible: (visible: boolean) => any;
  fetchGamesAgain: () => void;

  gamingPlatformsList: GamingPlatform[];
  isLoadingGamingPlatforms: boolean;
};

type GameFormData = {
  id: number;
  name: string;
  genre: string;
  platformId: number;
};

const GameFormModal: FunctionComponent<GameModalProps> = ({ gameModalData, visible, setVisible, fetchGamesAgain, gamingPlatformsList, isLoadingGamingPlatforms }) => {

  const [gameGenres, setGameGenres] = useState<string[]>([]);
  const gameId = gameModalData?.id;

  const { register, handleSubmit, errors, reset } = useForm<GameFormData>({ mode: 'onTouched' });

  const handleSaveGameSuccess = () => {
    toastr.success('Success', 'Game saved successfully!');
    close();
    fetchGamesAgain();
  };

  const [getGameGenres, { isLoading: isLoadingGameGenres }] = useApiService('game',
    (gameService) => gameService.getGameGenres(),
    (gameGenres) => setGameGenres(gameGenres),
  );

  const [addGame, { isLoading: isAddingGame }] = useApiService('game',
    (gameService, game: GameFormData) => gameService.addGame(game),
    handleSaveGameSuccess,
  );

  const [updateGame, { isLoading: isUpdatingGame }] = useApiService('game',
    (gameService, game: GameFormData) => gameService.updateGame(game.id, game),
    handleSaveGameSuccess,
  );

  const onSubmit = handleSubmit(data => {
    gameId
      ? updateGame({ ...data, id: gameId })
      : addGame(data);
  });

  const close = () => {
    setVisible(false);
  };

  const isSaving = isAddingGame || isUpdatingGame;

  useEffect(() => {
    if (visible) {
      if (gameModalData) {
        const formValues = {
          ...gameModalData,
          platformId: gameModalData.platform.id,
        };
        reset(formValues);
      } else {
        reset({});
      }
    } else {
      reset();
    }
  // eslint-disable-next-line
  }, [visible]);

  useEffectOnMount(() => {
    getGameGenres();
  });

  return (
    <Modal card visible={visible} setVisible={setVisible} width="500px">
      <ModalCardHeader title={gameId ? 'Update game' : 'Add game'} onClose={close} />
      <ModalCardBody>
        <form onSubmit={onSubmit}>
          <FormInput
            name="name"
            label="Name *"
            register={register}
            errors={errors}
            rules={{
              required: { value: true, message: 'The game\'s name is required' },
              maxLength: { value: 100, message: 'The game\'s name must not exceed 100 characters' }
            }}
          />
          <FormSelect
            name="genre"
            label="Genre *"
            options={gameGenres}
            showDefaultNullOption
            defaultNullOptionLabel="Select a genre..."
            isLoading={isLoadingGameGenres}
            register={register}
            errors={errors}
            rules={{
              required: { value: true, message: 'The game\'s genre is required' },
            }}
          />
          <FormSelect
            name="platformId"
            label="Gaming platform *"
            options={gamingPlatformsList}
            optionLabelField="name"
            optionValueField="id"
            showDefaultNullOption
            defaultNullOptionLabel="Select a gaming platform..."
            isLoading={isLoadingGamingPlatforms}
            register={register}
            errors={errors}
            rules={{
              required: { value: true, message: 'The game\'s gaming platform is required' },
            }}
          />
        </form>
      </ModalCardBody>
      <ModalCardFooter onConfirm={onSubmit} onCancel={close} isConfirming={isSaving} confirmButtonLabel="Save" />
    </Modal>
  );
}

export default GameFormModal;
