import React, { FunctionComponent } from "react";
import { useForm } from "react-hook-form";

import FormSelect from "../../../core/components/common/form/FormSelect";

import Modal from "../../../core/components/common/modal/Modal";
import ModalCardBody from "../../../core/components/common/modal/ModalCardBody";
import ModalCardFooter from "../../../core/components/common/modal/ModalCardFooter";
import ModalCardHeader from "../../../core/components/common/modal/ModalCardHeader";

import { Game } from "../../../domain/models/game";
import { Friend } from "../../../domain/models/friend";
import { useApiService } from "../../../domain/services/hooks/useApiService";
import { toastr } from "react-redux-toastr";

type GameLoanModalProps = {
  gameLoanModalData?: Game;
  visible: boolean;
  setVisible: (visible: boolean) => any;
  fetchGamesAgain: () => void;

  friendsList: Friend[];
  isLoadingFriends: boolean;
};

const GameLoanModal: FunctionComponent<GameLoanModalProps> = ({ gameLoanModalData, visible, setVisible, fetchGamesAgain, friendsList, isLoadingFriends }) => {

  const gameId = gameLoanModalData?.id;
  const isGameLoaned = Boolean(gameLoanModalData?.loanedTo);

  const { register, handleSubmit, errors, reset } = useForm<{ friendId: number }>({ mode: 'onTouched' });

  const [loanGame, { isLoading: isLoaningGame }] = useApiService('game',
    (gameService, { gameId, friendId }: { gameId: number, friendId: number }) => gameService.loanGameToFriend(gameId, friendId),
    () => {
      toastr.success('Success', 'Game loaned successfully');
      close();
      fetchGamesAgain();
    },
  );

  const [returnGame, { isLoading: isReturningGame }] = useApiService('game',
    (gameService, gameId: number) => gameService.returnGameFromFriend(gameId),
    () => {
      toastr.success('Success', 'Game returned successfully');
      close();
      fetchGamesAgain();
    },
  );

  const isSaving = isLoaningGame || isReturningGame;

  const onSubmit = handleSubmit(data => {
    isGameLoaned
      ? returnGame(gameId!)
      : loanGame({ gameId: gameId!, friendId: data.friendId })
  });

  const close = () => {
    reset();
    setVisible(false);
  }

  return (
    <Modal card visible={visible} setVisible={setVisible} width="350px">
      <ModalCardHeader title={isGameLoaned ? 'Return game' : 'Loan game to a friend'} onClose={close} />
      <ModalCardBody>
        {isGameLoaned && (
          <div className="has-text-centered">
            Do you really want to return game "{gameLoanModalData?.name}"?
          </div>
        )}
        {!isGameLoaned && (
          <form onSubmit={onSubmit}>
            <FormSelect
              name="friendId"
              label="Friend *"
              options={friendsList}
              optionLabelField="name"
              optionValueField="id"
              showDefaultNullOption
              defaultNullOptionLabel="Select a friend..."
              isLoading={isLoadingFriends}
              register={register}
              errors={errors}
              rules={{
                required: { value: true, message: 'To loan a game, please select a friend' },
              }}
            />
          </form>
        )}
      </ModalCardBody>
      <ModalCardFooter onConfirm={onSubmit} onCancel={close} isConfirming={isSaving} confirmButtonLabel={isGameLoaned ? 'Return' : 'Loan'} />
    </Modal>
  );
}

export default GameLoanModal;
