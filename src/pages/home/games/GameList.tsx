import { FunctionComponent, useState } from "react";

import { useEffectOnMount } from "../../../core/hooks/useEffectOnMount";

import { Game } from "../../../domain/models/game";
import { GamingPlatform } from "../../../domain/models/gamingPlatform";
import { Friend } from "../../../domain/models/friend";

import SectionSubtitle from "../SectionSubtitle";

import GameListItem from "./GameListItem";
import GameFormModal from "./GameFormModal";
import GameLoanModal from "./GameLoanModal";
import GameRemovalConfirmationModal from "./GameRemovalConfirmationModal";

type GameListProps = {
  gamesList: Game[];
  isLoading: boolean;
  fetchGames: () => any;

  gamingPlatformsList: GamingPlatform[];
  isLoadingGamingPlatforms: boolean;

  friendsList: Friend[];
  isLoadingFriends: boolean;
};

const GameList: FunctionComponent<GameListProps> = ({ gamesList, isLoading, fetchGames, gamingPlatformsList, isLoadingGamingPlatforms, friendsList, isLoadingFriends }) => {
  const [gameModalVisible, setGameModalVisible] = useState<boolean>(false);
  const [gameModalData, setGameModalData] = useState<Game>();

  const [gameLoanModalVisible, setGameLoanModalVisible] = useState<boolean>(false);
  const [gameLoanModalData, setGameLoanModalData] = useState<Game>();

  const [removeGameConfirmModalVisible, setRemoveGameConfirmModalVisible] = useState<boolean>(false);
  const [gameBeingRemoved, setGameBeingRemoved] = useState<Game>();

  const openAddGameModal = () => {
    setGameModalData(undefined);
    setGameModalVisible(true);
  };

  const openEditGameModal = (game: Game) => {
    setGameModalData(game);
    setGameModalVisible(true);
  };

  const openGameLoanModal = (game: Game) => {
    setGameLoanModalData(game);
    setGameLoanModalVisible(true);
  };

  const openRemoveGameConfirmationModal = (game: Game) => {
    setGameBeingRemoved(game);
    setRemoveGameConfirmModalVisible(true);
  };

  useEffectOnMount(() => {
    fetchGames();
  });

  return (
    <>
      <SectionSubtitle subtitle="My games" isLoading={isLoading} />

      <div className="content">
        <div className="mb-2">
          <button type="button" className="button is-primary is-fullwidth is-small has-icons-left" onClick={openAddGameModal}>
            <span className="icon">
              <span className="fas fa-plus"></span>
            </span>
            <span>Add game</span>
          </button>
        </div>

        {gamesList.length ? (
          <div className="columns is-multiline mt-1">
            {gamesList.map(game => (
              <div className="column is-half">
                <GameListItem
                  key={game.id}
                  game={game}
                  onEditButtonClick={openEditGameModal}
                  onRemoveButtonClick={openRemoveGameConfirmationModal}
                  onLoanButtonClick={openGameLoanModal}
                  onReturnButtonClick={openGameLoanModal}
                />
              </div>
            ))}
          </div>
        ) : (
          <div className="block has-text-centered">
            <small>You currently have no games added</small>
          </div>
        )}
      </div>

      <GameFormModal
        gameModalData={gameModalData}
        visible={gameModalVisible}
        setVisible={setGameModalVisible}
        fetchGamesAgain={fetchGames}

        gamingPlatformsList={gamingPlatformsList}
        isLoadingGamingPlatforms={isLoadingGamingPlatforms}
      />
      <GameLoanModal
        gameLoanModalData={gameLoanModalData}
        visible={gameLoanModalVisible}
        setVisible={setGameLoanModalVisible}
        fetchGamesAgain={fetchGames}

        friendsList={friendsList}
        isLoadingFriends={isLoadingFriends}
      />
      <GameRemovalConfirmationModal
        game={gameBeingRemoved}
        visible={removeGameConfirmModalVisible}
        setVisible={setRemoveGameConfirmModalVisible}
        fetchGamesAgain={fetchGames}
      />
    </>
  );
}

export default GameList;
