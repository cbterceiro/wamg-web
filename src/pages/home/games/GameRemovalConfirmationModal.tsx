import React, { FunctionComponent } from "react";
import { toastr } from "react-redux-toastr";
import ConfirmModal from "../../../core/components/common/modal/ConfirmModal";
import { Game } from "../../../domain/models/game";
import { useApiService } from "../../../domain/services/hooks/useApiService";

type GameRemovalConfirmationModalProps = {
  visible: boolean;
  setVisible: (visible: boolean) => any;
  game?: Game;
  fetchGamesAgain: () => void;
};

const GameRemovalConfirmationModal: FunctionComponent<GameRemovalConfirmationModalProps> = ({ visible, setVisible, game, fetchGamesAgain }) => {
  const [removeGame, { isLoading }] = useApiService('game',
    (gameService) => gameService.removeGame(game!.id),
    () => {
      toastr.success('Success', `Game removed successfully`);
      setVisible(false);
      fetchGamesAgain();
    }
  );

  return (
    <ConfirmModal
      visible={visible}
      setVisible={setVisible}
      title="Remove game"
      message={`Do you really want to remove game "${game?.name}"?`}
      isConfirming={isLoading}
      onConfirm={() => removeGame()}
      onCancel={() => setVisible(false)}
    />
  );
}

export default GameRemovalConfirmationModal;


