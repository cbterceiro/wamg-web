import classNames from "classnames";
import { FunctionComponent } from "react";
import { Game } from "../../../domain/models/game";

type GameListItemProps = {
  game: Game;
  onEditButtonClick: (game: Game) => void;
  onRemoveButtonClick: (game: Game) => void;
  onLoanButtonClick: (game: Game) => void;
  onReturnButtonClick: (game: Game) => void;
};

const GameListItem: FunctionComponent<GameListItemProps> = ({ game, onEditButtonClick, onRemoveButtonClick, onLoanButtonClick, onReturnButtonClick }) => {
  const isGameLoaned = Boolean(game.loanedTo);

  return (
    <div className="card">
      <div className="card-content pt-4 px-4 pb-2 has-text-centered">
        <div className="has-text-weight-semibold">
          {game.name}
        </div>
        <div className="mt-2 pt-1 is-size-7" style={{ borderTop: '1px solid #dedede' }}>
          {isGameLoaned ? (<span>Loaned to <strong>{game.loanedTo.name}</strong></span>) : `Currently not loaned` }
        </div>
      </div>
      <footer className="card-footer">
        <div className="card-footer-item is-clickable is-hoverable" onClick={() => onEditButtonClick(game)}>
          <span className="icon">
            <span className="fas fa-pen"></span>
          </span>
        </div>
        <div className="card-footer-item is-clickable is-hoverable" onClick={() => onRemoveButtonClick(game)}>
          <span className="icon">
            <span className="fas fa-trash"></span>
          </span>
        </div>
        <div className="card-footer-item is-clickable is-hoverable" onClick={() => isGameLoaned ? onReturnButtonClick(game) : onLoanButtonClick(game)}>
          <span className="icon">
            <span className={classNames('fas', { 'fa-undo': isGameLoaned, 'fa-people-arrows': !isGameLoaned })}></span>
          </span>
        </div>
      </footer>
    </div>
  );
};

export default GameListItem;
