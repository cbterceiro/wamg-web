import { FunctionComponent } from "react";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";

import classNames from 'classnames';

import { useSafeRedirect } from "../../core/hooks/useSafeRedirect";
import FormInput from "../../core/components/common/form/FormInput";

import { userSignIn } from "../../redux/actions/userActions";

import { useApiService } from "../../domain/services/hooks/useApiService";

type UserFormData = {
  username: string;
  password: string;
}

const LoginPage: FunctionComponent = () => {
  const { register, handleSubmit, errors } = useForm<UserFormData>({ mode: 'onTouched' });
  const dispatch = useDispatch();
  const redirectTo = useSafeRedirect();

  const handleSignInSuccess = (res: { accessToken: string }) => {
    dispatch(userSignIn(res));
    redirectTo('/');
  };

  const [signIn, { isLoading }] = useApiService('auth',
    (authService, formData: UserFormData) => authService.signIn(formData),
    handleSignInSuccess,
  );

  const onSubmit = handleSubmit(data => {
    signIn(data);
  });

  return (
    <div className="container p-4">
      <div className="block has-text-centered">
        <div className="block">
          <h1 className="title">Where Are My Games?</h1>
        </div>
        <div className="block">
          <p>The place to control your games, the gaming platforms you own and most importantly, to whom your games are loaned to.</p>
        </div>
        <div className="block">
          <p>Please sign in to continue</p>
        </div>
      </div>

      <div className="box mx-auto" style={{ maxWidth: '400px' }}>
        <form onSubmit={onSubmit}>
          <FormInput
            name="username"
            label="Username"
            register={register}
            errors={errors}
            rules={{
              required: { value: true, message: 'The username is required' },
              maxLength: { value: 50, message: 'The username must not exceed 50 characters' }
            }}
          />
          <FormInput
            type="password"
            name="password"
            label="Password"
            register={register}
            errors={errors}
            rules={{
              required: { value: true, message: 'The password is required' },
            }}
          />
          <div className="field">
            <div className="control">
              <button type="submit" className={classNames('button is-primary', { 'is-loading': isLoading })}>
                Sign in
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default LoginPage;
