// action types
export enum UserActionTypes {
  USER_SIGN_IN = "USER_SIGN_IN",
  USER_SIGN_OUT = "USER_SIGN_OUT",
}

// action payload
export type SignInActionPayload = {
  accessToken: string;
};

// actions
export type SignInAction = { type: typeof UserActionTypes.USER_SIGN_IN, payload: SignInActionPayload };
export type SignOutAction = { type: typeof UserActionTypes.USER_SIGN_OUT, };

export type UserActions = SignInAction | SignOutAction;

// action creators
export const userSignIn = (user: SignInActionPayload) => ({ type: UserActionTypes.USER_SIGN_IN, payload: { ...user } });
export const userSignOut = () => ({ type: UserActionTypes.USER_SIGN_OUT });
