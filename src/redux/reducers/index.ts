import { combineReducers } from "redux";

import { reducer as toastr } from "react-redux-toastr";

import user from "./userReducer";

const reducers = {
  user,
  toastr,
};

export const persistedStatePaths: (keyof typeof reducers)[] = [
  'user',
];

export const rootReducer = combineReducers(reducers);
