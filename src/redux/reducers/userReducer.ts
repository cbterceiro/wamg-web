import { UserState } from "../types";
import { UserActionTypes, UserActions } from "../actions/userActions";
import jwtDecode from "jwt-decode";

const initialState: UserState = {
  isAuthenticated: false,
};

type AccessTokenClaims = {
  unique_name: string;
};

export default function reducer(state = initialState, action: UserActions): UserState {
  switch (action.type) {
    case UserActionTypes.USER_SIGN_IN:
      const accessToken = action.payload.accessToken;
      const decodedJwt = jwtDecode<AccessTokenClaims>(accessToken);

      return {
        isAuthenticated: true,
        accessToken: accessToken,
        name: decodedJwt.unique_name,
      };

    case UserActionTypes.USER_SIGN_OUT:
      return {
        isAuthenticated: false,
      };

    default:
      return state;
  }
}
