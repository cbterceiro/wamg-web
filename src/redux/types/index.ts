import { ToastrState } from "react-redux-toastr";

export type RootState = {
  user: UserState;
  toastr: ToastrState;
};

export type UserState = {
  isAuthenticated: boolean;
  accessToken?: string;
  name?: string;
};
