import { RootState } from "../types";

export const selectUser = (state: RootState) => state.user;
export const selectIsUserAuthenticated = (state: RootState) => state.user.isAuthenticated;
export const selectAccessToken = (state: RootState) => state.user.accessToken;
export const selectUsername = (state: RootState) => state.user.name;
