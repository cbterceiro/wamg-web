import { FunctionComponent } from "react";
import { Provider } from "react-redux";
import ReduxToastr from "react-redux-toastr";

import store from "./redux/store";
import Router from "./pages/Router";

const App: FunctionComponent = () => {
  return (
    <Provider store={store}>
      <Router />
      <ReduxToastr
        timeOut={10000}
        newestOnTop={true}
        position="top-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar
        closeOnToastrClick
        preventDuplicates
      />
    </Provider>
  );
}

export default App;
