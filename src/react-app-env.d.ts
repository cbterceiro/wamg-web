/// <reference types="react-scripts" />

import * as Redux from "redux";
declare module 'redux-localstorage' {
  export default function persistState(paths: string | string[]): Redux.StoreEnhancer;
}
