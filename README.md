# WAMG Web App

**Where are my games?** If you are constantly running into this problem where you loan your games to your friends and keep losing track of them, then say no more fam, I got ya!

This is an SPA built on React that lets you control who are your friends, what are the gaming platforms you own and, of course, where are your games.

## Setting the environment
To run this project, you'll need:
- [Node.JS LTS](https://nodejs.org/en/download/)
- [NPM](https://www.npmjs.com/get-npm) (usually bundled with Node.js)

## Running the application
1. Clone the project's code on your machine
2. Run `npm start`
3. On your browser, open `http://localhost:3000` to see the application's welcome page

## Stopping the application
1. Simply run the `Ctrl+C` (or `Cmd+C` on Mac) on the Node.js local web server to cancel and stop the application

## Summary of technologies/dependencies used
This project was built using:
- React 17.0.1
- React Redux 7.2.2
- React Router DOM 5.2.0
- Typescript 4.1.3
- Node-Sass 4.14.1
- Bulma 0.9.1

## Architecture and general patterns
This project attempts to be a simple demonstration of a production-ready web app built with **React + Typescript** using:
- [Bulma](https://bulma.io) CSS Framework
- [React Redux](https://react-redux.js.org/)
- React [hooks](https://reactjs.org/docs/hooks-intro.html)
- Page routing with [React Router](https://reactrouter.com/)
- Client-side interaction with a REST Api using [axios](https://github.com/axios/axios)
- Full JWT-based token authentication handling
